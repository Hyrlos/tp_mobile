package com.example.tp3;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Exo2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exo2);

        /* Texte pris sur : https://developer.android.com/guide/topics/sensors/sensors_overview */
        TableLayout tl = findViewById(R.id.ex2_list);
        TextView t = new TextView(this);
        t.setText(R.string.missingCaptors);
        tl.addView(t);
        SensorManager sensorManager = (SensorManager)
                getSystemService(Context.SENSOR_SERVICE);
        if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)==null){
            TextView tv = new TextView(this);
            tv.setText(R.string.accelerometer);
            tl.addView(tv);
        }
        if (sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE)==null){
            TextView tv = new TextView(this);
            tv.setText(R.string.ambient_temperature);
            tl.addView(tv);
        }
        if (sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY)==null){
            TextView tv = new TextView(this);
            tv.setText(R.string.gravity);
            tl.addView(tv);
        }
        if (sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE)==null){
            TextView tv = new TextView(this);
            tv.setText(R.string.gyroscope);
            tl.addView(tv);
        }
        if (sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT)==null){
            TextView tv = new TextView(this);
            tv.setText(R.string.light);
            tl.addView(tv);
        }
        if (sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION)==null){
            TextView tv = new TextView(this);
            tv.setText(R.string.linear_acceleration);
            tl.addView(tv);
        }
        if (sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)==null){
            TextView tv = new TextView(this);
            tv.setText(R.string.magnetic_field);
            tl.addView(tv);
        }
        if (sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION)==null){
            TextView tv = new TextView(this);
            tv.setText(R.string.orientation);
            tl.addView(tv);
        }
        if (sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE)==null){
            TextView tv = new TextView(this);
            tv.setText(R.string.pressure);
            tl.addView(tv);
        }
        if (sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY)==null){
            TextView tv = new TextView(this);
            tv.setText(R.string.proximity);
            tl.addView(tv);
        }
        if (sensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY)==null){
            TextView tv = new TextView(this);
            tv.setText(R.string.relative_humidity);
            tl.addView(tv);
        }
        if (sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR)==null){
            TextView tv = new TextView(this);
            tv.setText(R.string.rotation_vector);
            tl.addView(tv);
        }
        if (sensorManager.getDefaultSensor(Sensor.TYPE_TEMPERATURE)==null){
            TextView tv = new TextView(this);
            tv.setText(R.string.temperature);
            tl.addView(tv);
        }
    }
}
