package com.example.tp3;

import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;
import java.lang.Math;

public class Exo4Activity extends AppCompatActivity implements SensorEventListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exo3);

        SensorManager mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Sensor mRotationSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        boolean supported = mSensorManager.registerListener(this, mRotationSensor, SensorManager.SENSOR_DELAY_UI);
        if (!supported) {
            mSensorManager.unregisterListener(this, mRotationSensor);
            Toast.makeText(getApplicationContext(), "Pas le matériel nécessaire", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        double x = 0, y = 0;

        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            x = Math.toDegrees(event.values[0]);
            y = Math.toDegrees(event.values[1]);
        }

        if (x < 0 && x >= -180) { // gauche
            Toast.makeText(getApplicationContext(), "Gauche !", Toast.LENGTH_LONG).show();
        } else { // droite
            Toast.makeText(getApplicationContext(), "Droite !", Toast.LENGTH_LONG).show();
        }
        if (y < 0 && y >= -180) { // haut
            Toast.makeText(getApplicationContext(), "Haut !", Toast.LENGTH_LONG).show();
        } else { // bas
            Toast.makeText(getApplicationContext(), "Bas !", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // TO DO, pas interessant pour nous
    }
}
