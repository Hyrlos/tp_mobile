package com.example.tp3;

import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.List;

import static java.lang.Character.getType;

public class Exo1Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exo1);

        final SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        List<Sensor> sensors = sensorManager.getSensorList(Sensor.TYPE_ALL);
        TableLayout tl = findViewById(R.id.ex1_list);
        for (Sensor sensor : sensors) {
            TableRow tr = new TableRow(this);
            TextView tv = new TextView(this);
            tv.setText(sensorToString(sensor));
            tr.addView(tv);
            tl.addView(tr);
        }
}

    private String sensorToString(Sensor s) {
        StringBuffer sensorDesc = new StringBuffer();
        sensorDesc.append("New sensor detected : \r\n");
        sensorDesc.append("\tName: " + s.getName() + "\r\n");
        sensorDesc.append("\tType: " + getType(s.getType()) + "\r\n");
        sensorDesc.append("Version: " + s.getVersion() + "\r\n");
        sensorDesc.append("Vendor: " + s.getVendor() + "\r\n");
        return sensorDesc.toString();
}
}
