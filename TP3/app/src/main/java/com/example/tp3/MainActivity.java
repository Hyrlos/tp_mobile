package com.example.tp3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((Button) findViewById(R.id.btnExo1)).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Start NewActivity.class
                        Intent myIntent = new Intent(MainActivity.this,
                                Exo1Activity.class);
                        startActivity(myIntent);
                    }
                });

        ((Button) findViewById(R.id.btnExo2)).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Start NewActivity.class
                        Intent myIntent = new Intent(MainActivity.this,
                                Exo2Activity.class);
                        startActivity(myIntent);
                    }
                });
        ((Button) findViewById(R.id.btnExo3)).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Start NewActivity.class
                        Intent myIntent = new Intent(MainActivity.this,
                                Exo3Activity.class);
                        startActivity(myIntent);
                    }
                });
        ((Button) findViewById(R.id.btnExo4)).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Start NewActivity.class
                        Intent myIntent = new Intent(MainActivity.this,
                                Exo4Activity.class);
                        startActivity(myIntent);
                    }
                });
        /*((Button) findViewById(R.id.btnExo5)).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Start NewActivity.class
                        Intent myIntent = new Intent(MainActivity.this,
                                Exo5Activity.class);
                        startActivity(myIntent);
                    }
                });
        ((Button) findViewById(R.id.btnExo6)).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Start NewActivity.class
                        Intent myIntent = new Intent(MainActivity.this,
                                Exo6MainActivity.class);
                        startActivity(myIntent);
                    }
                });*/
    }
}
