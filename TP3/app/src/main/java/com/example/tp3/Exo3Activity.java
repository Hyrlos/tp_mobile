package com.example.tp3;

import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;
import java.lang.Math;

public class Exo3Activity extends AppCompatActivity implements SensorEventListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exo3);

        SensorManager mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Sensor acc = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);


        boolean supported = mSensorManager.registerListener(this, acc,SensorManager.SENSOR_DELAY_GAME);
        if (!supported) {
            mSensorManager.unregisterListener(this, acc);
            Toast.makeText(getApplicationContext(), "Pas d’accelerometre", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        TextView tv = findViewById(R.id.color);
        double x = 0;

        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            x = Math.toDegrees(event.values[0]);
        }

        if (x <= 60 && x >= -60){
            tv.setBackgroundColor(Color.GREEN);
        } else if ((x > 60 && x <= 120) || (x < -60 && x >= -120)){
            tv.setBackgroundColor(Color.BLACK);
        } else {
            tv.setBackgroundColor(Color.RED);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // TO DO, pas interessant pour nous
    }
}
