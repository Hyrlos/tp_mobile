package com.example.tp1;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Exo6MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exo6);

        ((Button) findViewById(R.id.btn_validate)).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent myIntent = new Intent(Exo6MainActivity.this,
                                Exo6ResumeActivity.class);
                        Bundle extras =  new Bundle();

                        EditText edit = (EditText) findViewById(R.id.easyinterface_edit_nom);
                        String n = edit.getText().toString();
                        edit = (EditText) findViewById(R.id.easyinterface_edit_prenom);
                        String fn = edit.getText().toString();
                        edit = (EditText) findViewById(R.id.easyinterface_edit_age);
                        String a = (String) edit.getText().toString();
                        edit = (EditText) findViewById(R.id.easyinterface_edit_competence);
                        String c = (String) edit.getText().toString();
                        edit = (EditText) findViewById(R.id.easyinterface_edit_tel);
                        String p = (String) edit.getText().toString();

                        extras.putString("NAME", n);
                        extras.putString("FIRSTNAME", fn);
                        extras.putString("AGE", a);
                        extras.putString("DOMAIN", c);
                        extras.putString("PHONE", p);
                        myIntent.putExtras(extras);
                        startActivity(myIntent);
                    }
                });
    }

}
