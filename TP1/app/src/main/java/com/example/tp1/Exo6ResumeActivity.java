package com.example.tp1;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Exo6ResumeActivity extends AppCompatActivity {

    Dialog next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // get previous stored data
        Bundle extras = getIntent().getExtras();
        String name= extras.getString("NOM");
        String firstname= extras.getString("PRENOM");
        String age= extras.getString("AGE");
        String domain= extras.getString("DOMAINE");
        String phone= extras.getString("TEL");

        // make GUI great
        setContentView(R.layout.activity_exo6_resume);
        ((TextView) findViewById(R.id.resume_edit_nom)).append(name);
        ((TextView) findViewById(R.id.resume_edit_prenom)).append(firstname);
        ((TextView) findViewById(R.id.resume_edit_age)).append(age);
        ((TextView) findViewById(R.id.resume_edit_competence)).append(domain);
        ((TextView) findViewById(R.id.resume_edit_tel)).append(phone);

        // deal with the next button
        ((Button) findViewById(R.id.btn_validate)).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        next = onCreateDialog(new Bundle());
                        next.show();
                    }
                });
    }

    public Dialog onCreateDialog(Bundle savedInstanceState){
        AlertDialog.Builder builder = new AlertDialog.Builder(Exo6ResumeActivity.this);
        builder.setMessage(R.string.dialog);
        builder.setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent myIntent = new Intent(Exo6ResumeActivity.this,
                        Exo6EmptyActivity.class);
                startActivity(myIntent);
            }
        });
        builder.setNegativeButton(R.string.dialog_no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent myIntent = new Intent(Exo6ResumeActivity.this,
                        Exo6MainActivity.class);
                startActivity(myIntent);
            }
        });

        return builder.create();
    }
}
