package com.example.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Exo7MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exo7_main);

        ((Button) findViewById(R.id.btn_validate)).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent myIntent = new Intent(Exo7MainActivity.this,
                                Exo7ResumeActivity.class);
                        Bundle extras =  new Bundle();

                        EditText edit = (EditText) findViewById(R.id.easyinterface_edit_nom);
                        String n = edit.getText().toString();
                        edit = (EditText) findViewById(R.id.easyinterface_edit_prenom);
                        String fn = edit.getText().toString();
                        edit = (EditText) findViewById(R.id.easyinterface_edit_age);
                        String a = (String) edit.getText().toString();
                        edit = (EditText) findViewById(R.id.easyinterface_edit_competence);
                        String c = (String) edit.getText().toString();
                        edit = (EditText) findViewById(R.id.easyinterface_edit_tel);
                        String p = (String) edit.getText().toString();

                        extras.putString("NOM", n);
                        extras.putString("PRENOM", fn);
                        extras.putString("AGE", a);
                        extras.putString("DOMAINE", c);
                        extras.putString("TEL", p);
                        myIntent.putExtras(extras);
                        startActivity(myIntent);
                    }
                });
    }
}
