package com.example.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Exo8MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exo8_main);

        ((Button) findViewById(R.id.train_btn)).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent myIntent = new Intent(Exo8MainActivity.this,
                                Exo8ResultActivity.class);
                        Bundle extras =  new Bundle();

                        EditText edit = (EditText) findViewById(R.id.train_from);
                        String f = edit.getText().toString();
                        edit = (EditText) findViewById(R.id.train_to);
                        String to = edit.getText().toString();

                        extras.putString("De", f);
                        extras.putString("Pour", to);
                        myIntent.putExtras(extras);
                        startActivity(myIntent);
                    }
                });
    }
}
