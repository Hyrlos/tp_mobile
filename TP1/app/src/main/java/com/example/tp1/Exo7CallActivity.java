package com.example.tp1;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Exo7CallActivity extends AppCompatActivity {

    String phone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exo7_call);


        Bundle extras = getIntent().getExtras();

        phone= extras.getString("TEL");

        ((TextView) findViewById(R.id.phone_call)).append(phone);

        ((Button) findViewById(R.id.btn_call)).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:"+phone));
                        startActivity(callIntent);
                    }
                });
    }
}
