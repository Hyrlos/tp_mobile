package com.example.tp1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Exo8ResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exo8_result);

        Bundle extras = getIntent().getExtras();
        String f= extras.getString("DE");
        String t= extras.getString("POUR");
        // make GUI great
        setContentView(R.layout.activity_exo8_result);
        ((TextView) findViewById(R.id.train_from)).append(f);
        ((TextView) findViewById(R.id.train_to)).append(t);
    }
}
