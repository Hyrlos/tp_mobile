package com.example.tp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ContactOpenHelper extends SQLiteOpenHelper {

    private static final int BASE_VERSION = 1;
    private static final String BASE_NOM = "planetes.db";
    private static final String TABLE_CONTACTS = "table_contacts";
    private static final String COLONNE_ID = "_id";
    private static final String COLONNE_NOM = "nom";
    private static final String COLONNE_PRENOM = "prenom";
    private static final String COLONNE_TEL = "tel";

    private static final String REQUETE_CREATION_BD = "create table "
            + TABLE_CONTACTS + " (" + COLONNE_ID
            + " integer primary key autoincrement, " + COLONNE_NOM
            + " text not null, " + COLONNE_PRENOM + " text not null, " + COLONNE_TEL + " text not null);";


    public ContactOpenHelper(Context context, String nom, SQLiteDatabase.CursorFactory cursorfactory, int version) {
        super(context, nom, cursorfactory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(REQUETE_CREATION_BD);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table " + TABLE_CONTACTS + ";");
        onCreate(db);
    }
}
