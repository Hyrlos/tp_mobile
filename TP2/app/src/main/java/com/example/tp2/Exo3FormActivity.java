package com.example.tp2;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileOutputStream;

public class Exo3FormActivity extends AppCompatActivity {

    int cpt;
    String FILENAME = "contacts_file";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if ((savedInstanceState != null) && (savedInstanceState.containsKey("COMPTEUR"))) {
            cpt = savedInstanceState.getInt("COMPTEUR");
        } else {
            cpt = 0;
        }

        setContentView(R.layout.activity_exo3_form);

        ((TextView)findViewById(R.id.cpt)).setText(String.valueOf(cpt));

        ((Button) findViewById(R.id.btn_validate)).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        EditText edit = (EditText) findViewById(R.id.ex3_edit_nom);
                        String name = edit.getText().toString();
                        edit = (EditText) findViewById(R.id.ex3_edit_prenom);
                        String firstname = edit.getText().toString();
                        edit = (EditText) findViewById(R.id.ex3_edit_tel);
                        String phone = (String) edit.getText().toString();

                        String c = name+"_"+firstname+"_"+phone;
                        FileOutputStream fos = null;
                        try {
                            fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
                            fos.write(c.getBytes());
                            fos.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        Intent myIntent = new Intent(Exo3FormActivity.this,
                                Exo3ListActivity.class);

                        Toast.makeText(Exo3FormActivity.this, "Attention, le fichier contenant le contact est écrasé à chaque fois",Toast.LENGTH_SHORT).show();
                        startActivity(myIntent);
                    }
                });
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        if(savedInstanceState == null){
            savedInstanceState = new Bundle();
        }
        cpt++;
        savedInstanceState.putInt("COMPTEUR", cpt);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if ((savedInstanceState != null) && (savedInstanceState.containsKey("COMPTEUR"))) {
            cpt = savedInstanceState.getInt("COMPTEUR");
        } else {
            cpt = 0;
        }
        ((TextView)findViewById(R.id.cpt)).setText(String.valueOf(cpt));
    }

    @Override
    protected void onResume(){
        super.onResume();
        ((TextView)findViewById(R.id.cpt)).setText(String.valueOf(cpt));
    }
}
