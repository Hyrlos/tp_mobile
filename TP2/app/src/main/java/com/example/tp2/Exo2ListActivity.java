package com.example.tp2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Exo2ListActivity extends AppCompatActivity {

    ArrayList<String> contacts = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        String name= extras.getString("NAME");
        String firstname= extras.getString("FIRSTNAME");
        String phone= extras.getString("PHONE");

        String c = name+"_"+firstname+"_"+phone;
        contacts.add("TMP1");
        contacts.add("TMP2");
        contacts.add(c);
        setContentView(R.layout.activity_exo2_list);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, contacts);

        ListView listView = (ListView) findViewById(R.id.ex2_listview);
        listView.setAdapter(adapter);
    }

}
