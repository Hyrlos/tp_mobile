package com.example.tp2;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.CharBuffer;
import java.util.ArrayList;

public class Exo3ListActivity extends AppCompatActivity {

    ArrayList<String> contacts = new ArrayList<String>();
    String FILENAME = "contacts_file";
    ContactDB db = new ContactDB(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exo3_list);

        // récupère les contacts du fichiers
        try {
            FileInputStream fIn = openFileInput(FILENAME) ;
            InputStreamReader isr = new InputStreamReader(fIn) ;
            BufferedReader buffreader = new BufferedReader(isr) ;

            String readString = buffreader.readLine() ;
            while ( readString != null ) {
                contacts.add(readString);
                readString = buffreader.readLine() ;
            }
            isr.close();
            fIn.close();
        } catch ( IOException ioe ) {
            ioe.printStackTrace() ;
        }

        // Vérifie si le contact est dans la DB
        for (String c_str : contacts) {

            String[] split = c_str.split("_");
            String nom = split[0];
            String prenom = split[1];
            String tel = split[2];

            Contact c = new Contact(nom, prenom, tel);
            // Si non, l'ajoute
            db.open();
            Cursor result = db.getContact(c);
            if(result == null){
                db.insertContact(c);
            }
        }


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, contacts);

        ListView listView = (ListView) findViewById(R.id.ex3_listview);
        listView.setAdapter(adapter);
    }

}
