package com.example.tp2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Exo1FormActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exo1);

        ((Button) findViewById(R.id.btn_validate)).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent myIntent = new Intent(Exo1FormActivity.this,
                                Exo1ListActivity.class);

                        Bundle extras =  new Bundle();

                        EditText edit = (EditText) findViewById(R.id.ex1_edit_nom);
                        String n = edit.getText().toString();
                        edit = (EditText) findViewById(R.id.ex1_edit_prenom);
                        String fn = edit.getText().toString();
                        edit = (EditText) findViewById(R.id.ex1_edit_tel);
                        String p = (String) edit.getText().toString();

                        extras.putString("NAME", n);
                        extras.putString("FIRSTNAME", fn);
                        extras.putString("PHONE", p);
                        myIntent.putExtras(extras);

                        startActivity(myIntent);
                    }
                });
    }
}
