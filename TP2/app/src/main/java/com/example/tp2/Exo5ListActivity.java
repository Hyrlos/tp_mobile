package com.example.tp2;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class Exo5ListActivity extends AppCompatActivity {

    ArrayList<String> contacts = new ArrayList<String>();
    ContactDB db = new ContactDB(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exo5_list);

        db.open();
        Cursor cursor = db.getAllContacts();


        if (cursor != null ) {
            if  (cursor.moveToFirst()) {
                do {
                    String nom = cursor.getString(0);//cursor.getColumnIndex("COLONNE_NOM"));
                    String prenom = cursor.getString(1);//cursor.getColumnIndex("COLONNE_PRENOM"));
                    String tel = cursor.getString(2);//cursor.getColumnIndex("COLONNE_TEL"));
                    contacts.add(""+ nom + "_" + prenom + "_" + tel);
                }while (cursor.moveToNext());
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, contacts);

        ListView listView = (ListView) findViewById(R.id.ex5_listview);
        listView.setAdapter(adapter);
    }

}
