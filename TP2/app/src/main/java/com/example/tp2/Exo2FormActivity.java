package com.example.tp2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Exo2FormActivity extends AppCompatActivity {

    int cpt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if ((savedInstanceState != null) && (savedInstanceState.containsKey("COMPTEUR"))) {
            cpt = savedInstanceState.getInt("COMPTEUR");
        } else {
            cpt = 0;
        }

        setContentView(R.layout.activity_exo2_form);

        ((TextView)findViewById(R.id.cpt)).setText(String.valueOf(cpt));

        ((Button) findViewById(R.id.btn_validate)).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent myIntent = new Intent(Exo2FormActivity.this,
                                Exo2ListActivity.class);

                        Bundle extras =  new Bundle();

                        EditText edit = (EditText) findViewById(R.id.ex2_edit_nom);
                        String n = edit.getText().toString();
                        edit = (EditText) findViewById(R.id.ex2_edit_prenom);
                        String fn = edit.getText().toString();
                        edit = (EditText) findViewById(R.id.ex2_edit_tel);
                        String p = (String) edit.getText().toString();

                        extras.putString("NAME", n);
                        extras.putString("FIRSTNAME", fn);
                        extras.putString("PHONE", p);
                        myIntent.putExtras(extras);

                        startActivity(myIntent);
                    }
                });
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        if(savedInstanceState == null){
            savedInstanceState = new Bundle();
        }
        cpt++;
        savedInstanceState.putInt("COMPTEUR", cpt);
        //Toast.makeText(this, "onSaveInstanceState",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // Toast.makeText(this, "restore",Toast.LENGTH_SHORT).show();

        if ((savedInstanceState != null) && (savedInstanceState.containsKey("COMPTEUR"))) {
            cpt = savedInstanceState.getInt("COMPTEUR");
        } else {
            cpt = 0;
        }
        ((TextView)findViewById(R.id.cpt)).setText(String.valueOf(cpt));
    }

    @Override
    protected void onResume(){
        super.onResume();
        ((TextView)findViewById(R.id.cpt)).setText(String.valueOf(cpt));
        // Toast.makeText(this, "resume",Toast.LENGTH_SHORT).show();
    }
}
