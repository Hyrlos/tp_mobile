package com.example.tp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class ContactDB {
    private static final int BASE_VERSION = 1;
    private static final String BASE_NOM = "planetes.db";
    private static final String TABLE_CONTACTS = "table_contacts";
    private static final String COLONNE_ID = "_id";
    public static final int COLONNE_ID_ID = 0;
    private static final String COLONNE_NOM = "nom";
    public static final int COLONNE_NOM_ID = 1;
    private static final String COLONNE_PRENOM = "prenom";
    public static final int COLONNE_PRENOM_ID = 2;
    private static final String COLONNE_TEL = "tel";
    private static final int COLONNE_TEL_ID = 3;

    private SQLiteDatabase db;
    private ContactOpenHelper myOpenHelper;


    public ContactDB(Context context){
        //On crée la BDD et sa table
        myOpenHelper = new ContactOpenHelper(context, BASE_NOM, null, BASE_VERSION);
    }

    public void open(){
        //on ouvre la BDD en écriture
        db = myOpenHelper.getWritableDatabase();
    }

    public void close(){
        //on ferme l'accès à la BDD
        db.close();
    }

    public SQLiteDatabase getDB(){
        return db;
    }

    public Cursor getContact(Contact contact){
        Cursor c = db.query(TABLE_CONTACTS,
            new String[] {
                COLONNE_ID, COLONNE_NOM, COLONNE_PRENOM, COLONNE_TEL },
                null, null, null,
                COLONNE_NOM + " LIKE " + contact.getNom() + " AND " + COLONNE_PRENOM +
                       " LIKE " + contact.getPrenom() + " AND " + COLONNE_TEL + " LIKE " + contact.getTel(), null);
        return c;
    }

    public Cursor getAllContacts() {
        return db.query(TABLE_CONTACTS, new String[] {
                        COLONNE_NOM, COLONNE_PRENOM, COLONNE_TEL },
                null, null, null, null, null);
    }


    public boolean insertContact(Contact c){
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLONNE_NOM, c.getNom());
        contentValues.put(COLONNE_PRENOM, c.getPrenom());
        contentValues.put(COLONNE_TEL, c.getTel());
        return (db.insert(TABLE_CONTACTS, null, contentValues) != -1 );
    }

    public Contact cursorToContact(Cursor c){
        //si aucun élément n'a été retourné dans la requête, on renvoie null
        if (c.getCount() == 0)
            return null;

        //Sinon on se place sur le premier élément
        c.moveToFirst();
        //On créé un contact
        Contact contact = new Contact();
        //on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
        contact.setNom(c.getString(COLONNE_NOM_ID));
        contact.setPrenom(c.getString(COLONNE_PRENOM_ID));
        contact.setTel(c.getString(COLONNE_TEL_ID));
        //On ferme le cursor
        c.close();

        return contact;
    }
}
