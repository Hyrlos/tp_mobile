package com.example.tp2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Exo4FormActivity extends AppCompatActivity {

    int cpt;
    ContactDB db = new ContactDB(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exo4_form);

        if ((savedInstanceState != null) && (savedInstanceState.containsKey("COMPTEUR"))) {
            cpt = savedInstanceState.getInt("COMPTEUR");
        } else {
            cpt = 0;
        }

        db.open();

        ((TextView)findViewById(R.id.cpt)).setText(String.valueOf(cpt));
        ((Button) findViewById(R.id.btn_validate)).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        EditText edit = (EditText) findViewById(R.id.ex4_edit_nom);
                        String name = edit.getText().toString();
                        edit = (EditText) findViewById(R.id.ex4_edit_prenom);
                        String firstname = edit.getText().toString();
                        edit = (EditText) findViewById(R.id.ex4_edit_tel);
                        String phone = (String) edit.getText().toString();

                        Contact contact = new Contact(name, firstname, phone);
                        db.insertContact(contact);

                        Intent myIntent = new Intent(Exo4FormActivity.this,
                                Exo4ListActivity.class);

                        startActivity(myIntent);
                    }
                });
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        if(savedInstanceState == null){
            savedInstanceState = new Bundle();
        }
        cpt++;
        savedInstanceState.putInt("COMPTEUR", cpt);
        //db.close();
    }

    @Override
    protected void onResume(){
        super.onResume();
        ((TextView)findViewById(R.id.cpt)).setText(String.valueOf(cpt));
        //db.open();
    }
}
